FROM python:3.10

ENV PYPISERVER_PORT=3141

RUN pip install -U pip \
    && pip install -U devpi \
    && mkdir -p /data/packages \
    && devpi-init --serverdir /data/packages \
    && nohup bash -c "devpi-server --host 0.0.0.0 --port ${PYPISERVER_PORT} --serverdir /data/packages &" \
    && sleep 5 \
    && devpi use "http://0.0.0.0:${PYPISERVER_PORT}" \
    && devpi login root --password="" \
    && devpi index  pypi type=mirror 

VOLUME /data/packages

EXPOSE $PYPISERVER_PORT

ENTRYPOINT  devpi-server --host 0.0.0.0 --port $PYPISERVER_PORT --serverdir /data/packages

